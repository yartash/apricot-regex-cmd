﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace ApricotRegexCommand
{
    public class MatchResult
    {
        [JsonProperty("length")]
        public int Length { get; set; }

        [JsonProperty("index")]
        public int Index { get; set; }

        [JsonProperty("match")]
        public string Match { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("groups")]
        public List<string> Groups { get; set; }

        public MatchResult()
        {
            Groups = new List<string>();
        }
    }
}