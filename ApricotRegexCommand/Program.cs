﻿using System;

namespace ApricotRegexCommand
{
    class Program
    {
        static void Main(string[] args)
        {
            var executer = new RegexExecute(args);

            Console.WriteLine(executer.Run());
        }
    }
}