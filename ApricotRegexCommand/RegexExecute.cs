﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace ApricotRegexCommand
{
    public class RegexExecute
    {
        public const string OptionPatternKey = "-p:";
        public const string OptionContentKey = "-c:";
        public const string OptionFlagsKey = "-f:";
        public const string OptionFileKey = "-o";

        private string _pattern;
        private string _content;
        private RegexOptions _flags = RegexOptions.None;
        private string _error = "";

        public RegexExecute(IEnumerable<string> options) 
        {
            ParseOption(options);
        }

        public string Run()
        {
            if (_error != "")
            {
                return _error;
            }

            var regex = new Regex(_pattern, _flags);

            return PopulateResult(regex.Matches(_content));
        }

        private static string PopulateResult(ICollection collection)
        {
            var result = new Result();

            if (collection.Count == 0)
            {
                result.Message = "Not matching";
                result.Status = "error";
            }
            else
            {
                result.Status = "success";

                foreach (Match match in collection)
                {
                    var groups = (from Group @group in match.Groups select @group.Value).ToList();

                    result.Matches.Add(
                        new MatchResult{
                            Index = match.Index,
                            Length = match.Length,
                            Match = match.Value,
                            Name = match.Name,
                            Groups = groups
                        }
                    );
                }
            }

            return JsonConvert.SerializeObject(result);
        }

        private void ParseOption(IEnumerable<string> options)
        {
            if (!options.Any())
            {
                _error = JsonConvert.SerializeObject(
                    new Result { Message = "All options is required.", Status = "error" }
                );

                return;
            }

            foreach (var option in options)
            {
                if (option.StartsWith(OptionPatternKey))
                {
                    _pattern = option
                        .Replace(OptionPatternKey, "")
                        .Replace("&#10;", "\n")
                        .Replace("&#11;", "\r")
                        .Replace("&#12;", "\"");
                }
                else if (option.StartsWith(OptionContentKey))
                {
                    _content = option
                        .Replace(OptionContentKey, "")
                        .Replace("&#10;", "\n")
                        .Replace("&#11;", "\r")
                        .Replace("&#12;", "\"");
                    _content = WebUtility.HtmlDecode(_content);
                }
                else if (option.StartsWith(OptionFlagsKey))
                {
                    _flags = (RegexOptions) Enum.ToObject(
                        typeof(RegexOptions), 
                        Convert.ToInt32(option.Replace(OptionFlagsKey, ""))
                    );
                }
                else if (option.StartsWith(OptionFileKey))
                {
                    if (!File.Exists(_content))
                    {
                        continue;
                    }

                    var path = _content;

                    _content = File
                        .ReadAllText(path)
                        .Replace("&#10;", "\n")
                        .Replace("&#11;", "\r")
                        .Replace("&#12;", "\"");
                    File.Delete(path);
                }
            }
        }
    }
}