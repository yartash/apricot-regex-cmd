﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace ApricotRegexCommand
{
    public class Result
    {
        [JsonProperty("matches")]
        public List<MatchResult> Matches { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        public Result()
        {
            Matches = new List<MatchResult>();
        }
    }
}